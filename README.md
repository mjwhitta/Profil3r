# Profil3r

Profil3r is an [OSINT] tool that allows you to find potential profiles
of a person on social networks, as well as their email addresses. This
program also alerts you to the presence of a data leak for the found
emails.

![](doc/demo_cli.gif)

[OSINT]: https://en.wikipedia.org/wiki/Open-source_intelligence

## Prerequisites

- [Python3](https://www.python.org/)
- Poetry

    ```
    $ python3 -m pip install --user poetry
    ```

## Installation

#### Build from source

```
$ git clone https://gitlab.com/mjwhitta/Profil3r.git
$ ./Profil3r/scripts/install.sh
```

## Features

### Domain

- [x] TLD (.com, .org, .net, etc...)

### Emails

- [x] Data leaks
- [x] Emails

### Social

Service    | Profile Scraping
-------    | ----------------
Chirpty    | Yes
DeviantArt | No
Ello       | No
Facebook   | No
Flickr     | Yes
GoodReads  | No
Instagram  | Yes
Linktr.ee  | Yes
MySpace    | Yes
Pinterest  | No
Tiktok     | No
Twitter    | Yes

### Music

Service    | Profile Scraping
-------    | ----------------
bandcamp   | Yes
Smule      | No
Soundcloud | No
Spotify    | No

### Programming

Service    | Profile Scraping
-------    | ----------------
Asciinema  | No
CodeMentor | No
Cracked.to | No
Github     | Yes
Ifttt      | No
LessWrong  | Yes
NPM        | Yes
Pastebin   | Yes
PyPi       | Yes
Repl.it    | No

### Forum

Service       | Profile Scraping
-------       | ----------------
0x00sec.org   | No
Hackernews    | Yes
Jeuxvideo.com | Yes

### Tchat

Service | Profile Scraping
------- | ----------------
Skype   | No

### Entertainment

Service     | Profile Scraping
-------     | ----------------
Dailymotion | No
DeviantArt  | Yes
Dribbble    | Yes
Vimeo       | No

### Porn

Service         | Profile Scraping
--------------- | ----------------
PornHub         | Yes
RedTube         | No
XVideos         | No

### Money

Service      | Profile Scraping
-------      | ----------------
BuyMeACoffee | No
CashApp      | No
Patreon      | No

### Web Hosting

Service    | Profile Scraping
-------    | ----------------
AboutMe    | Yes
SlideShare | Yes
WordPress  | No

### Gaming

Service    | Profile Scraping
-------    | ----------------
Dota2      | Yes
Kongregate | Yes
Op.gg      | Yes
Steam      | No

### Medias

Service     | Profile Scraping
-------     | ----------------
Dev.to      | No
Hubpages    | Yes
LiveJournal | No
Medium      | No

### Travel

Service     | Profile Scraping
-------     | ----------------
TripAdvisor | No

### Collaborative

Service       | Profile Scraping
-------       | ----------------
Instructables | Yes
Wikipedia     | No

### CTF

Service | Profile Scraping
------- | ----------------
Root-me | Yes

### Privacy

Service | Profile Scraping
------- | ----------------
Keybase | Yes

## Report

To further analyze the data collected by Profil3r, it is possible to
generate reports in different formats using the argument `-r/--report
<path of the reports>`.

### JSON

A report in JSON format is generated in the `reports/json` folder.

### CSV

A report in CSV format is generated in the `reports/csv` folder.

### HTML

A report in HTML format is generated in the `reports/html` folder, you
can access it in your webbrowser.

![](doc/demo_web.gif)

## Usage

```
usage: profil3r [-h] -p PROFILE [PROFILE ...] [-r REPORT]

optional arguments:
  -h, --help            show this help message and exit
  -p PROFILE [PROFILE ...], --profile PROFILE [PROFILE ...]
                        parts of the username that you are looking for, e.g. : john doe
  -r REPORT, --report REPORT
                        path to the report directory, e.g. : ./OSINT
```

## Example

```
$ profil3r -p john doe -r ./OSINT
```

## License

This project is under the MIT license.

## Contact

for any remark, suggestion or job offer, you can contact me at
r0g3r5@protonmail.com or on twitter
[@Rog3rSm1th](https://twitter.com/Rog3rSm1th)
