#!/usr/bin/env bash

pip3() { python3 -m pip "$@"; }

cd "$(dirname "$0")/.." || exit 1

./scripts/uninstall.sh

echo "[+] Building..."
poetry install
poetry build

echo "[+] Installing Profil3r..."
pip3 install --no-deps --user ./dist/*.whl

echo "[+] Installation completed"
