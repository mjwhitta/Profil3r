#!/usr/bin/env bash

pip3() { python3 -m pip "$@"; }

cd "$(dirname "$0")/.." || exit 1

echo "[+] Deleting older sources..."
rm -fr "$(pip3 show profil3r | awk '/Location:/ {print $2}')/profil3r"

echo "[+] Uninstalling previous versions of Profil3r..."
yes | pip3 uninstall profil3r

echo "[+] Deleting older wheels..."
rm -fr ./dist
